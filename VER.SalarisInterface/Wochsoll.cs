using Algemeen;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
namespace VER.SalarisInterface
{
	internal class Wochsoll
	{
		public static List<Wochsoll> lwWochsoll = new List<Wochsoll>();
		private string sSalnum;
		public int wochsoll;
		public int filler;
		private int week;
		public Wochsoll(string sSalnum, DateTime dtDatum)
		{
            Parameter.SMandant = "";
			this.sSalnum = sSalnum;
			List<string> list = new List<string>();
			list.Add("-s");
			list.Add("-d;");
			list.Add("-c" + dtDatum.ToString("yyMMdd"));
			FileInfo fiFile = Proces.FiSqlsel("personeel.wochsoll personeel.filler2 from personeel where salnum = '" + sSalnum + "'", list);
			string text = Proces.SLeesFile(fiFile);
			int num = 0;
			int num2 = 0;
            if (text.Contains(";"))
            {
                int.TryParse(text.Split(new char[]
			    {
				    ';'
			    })[0], out num);
            }
			int num3 = Convert.ToInt32(Convert.ToDecimal(num % 60) / 60m * 100m);
			int num4 = num / 60;
			num = num4 * 100 + num3;
            if (text.Contains(";"))
            {
                int.TryParse(text.Split(new char[]
			    {
				    ';'
			    }   )[1], out num2);
            }
			int num5 = Convert.ToInt32(Convert.ToDecimal(num2 % 60) / 60m * 100m);
			int num6 = num2 / 60;
			num2 = num6 * 100 + num5;
			this.wochsoll = num;
			this.filler = num2;
			CultureInfo cultureInfo = new CultureInfo("nl-BE");
			Calendar calendar = cultureInfo.Calendar;
			CalendarWeekRule calendarWeekRule = cultureInfo.DateTimeFormat.CalendarWeekRule;
			DayOfWeek firstDayOfWeek = cultureInfo.DateTimeFormat.FirstDayOfWeek;
			int weekOfYear = calendar.GetWeekOfYear(dtDatum, calendarWeekRule, firstDayOfWeek);
			DayOfWeek dayOfWeek = new DateTime(dtDatum.Year, 12, 31).DayOfWeek;
			if (weekOfYear == 1 && dayOfWeek != DayOfWeek.Sunday)
			{
				this.week = calendar.GetWeekOfYear(new DateTime(dtDatum.Year, 12, 31), calendarWeekRule, firstDayOfWeek);
			}
			else
			{
				this.week = weekOfYear;
			}
			if (weekOfYear > 53)
			{
			}
			Wochsoll.lwWochsoll.Add(this);
            Parameter.SMandant = "bel";
		}
		public static Wochsoll wZoekBijlage(string sSalnum, int week)
		{
			return Wochsoll.lwWochsoll.Find((Wochsoll w) => w.sSalnum == sSalnum && w.week == week);
		}
	}
}
