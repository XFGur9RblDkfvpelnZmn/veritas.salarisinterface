using Algemeen;
using System;
using System.Collections.Generic;
using System.Globalization;
namespace VER.SalarisInterface
{
	internal class Record : IComparable
	{
		public string sSalnum;
		public string sWerkgeverNr;
		public string sDatum;
		public string sLooncode;
		public int iUren;
		public static List<Record> lrRecords = new List<Record>();
		public static List<Record> lrNewRecords = new List<Record>();
		public int week;
		public DateTime dtDatum;
		public Record(string sSalnum, string sWerkgeverNr, string sDatum, string sLooncode, string sUren)
		{
			while (sSalnum.Length < 7)
			{
				sSalnum = "0" + sSalnum;
			}
			while (sSalnum.Length > 7)
			{
				sSalnum = sSalnum.Substring(1);
			}
			this.sSalnum = sSalnum;
			this.sWerkgeverNr = sWerkgeverNr;
			this.sDatum = sDatum;
			this.sLooncode = sLooncode;
			try
			{
				this.iUren = Convert.ToInt32(sUren);
			}
			catch (Exception ex)
			{
				this.iUren = 0;
				Log.Verwerking("record niet correct: " + sSalnum + " | " + sDatum);
				Log.Exception(ex);
			}
			this.dtDatum = Proces.dtStringToDate(sDatum);
			CultureInfo cultureInfo = new CultureInfo("nl-BE");
			Calendar calendar = cultureInfo.Calendar;
			CalendarWeekRule calendarWeekRule = cultureInfo.DateTimeFormat.CalendarWeekRule;
			DayOfWeek firstDayOfWeek = cultureInfo.DateTimeFormat.FirstDayOfWeek;
			int weekOfYear = calendar.GetWeekOfYear(this.dtDatum, calendarWeekRule, firstDayOfWeek);
			DayOfWeek dayOfWeek = new DateTime(this.dtDatum.Year, 12, 31).DayOfWeek;
			if (weekOfYear == 1 && dayOfWeek != DayOfWeek.Sunday)
			{
				this.week = calendar.GetWeekOfYear(new DateTime(this.dtDatum.Year, 12, 31), calendarWeekRule, firstDayOfWeek);
			}
			else
			{
				this.week = weekOfYear;
			}
			if (weekOfYear > 53)
			{
			}
			Record.lrRecords.Add(this);
		}
		public Record(string sSalnum, string sWerkgeverNr, string sDatum, string sLooncode, string sUren, bool bGeenLijst)
		{
			while (sSalnum.Length < 7)
			{
				sSalnum = "0" + sSalnum;
			}
			while (sSalnum.Length > 7)
			{
				sSalnum = sSalnum.Substring(1);
			}
			this.sSalnum = sSalnum;
			this.sWerkgeverNr = sWerkgeverNr;
			this.sDatum = sDatum;
			this.sLooncode = sLooncode;
			try
			{
				this.iUren = Convert.ToInt32(sUren);
			}
			catch (Exception ex)
			{
				this.iUren = 0;
				Log.Verwerking("record niet correct: " + sSalnum + " | " + sDatum);
				Log.Exception(ex);
			}
			this.dtDatum = Proces.dtStringToDate(sDatum);
			CultureInfo cultureInfo = new CultureInfo("nl-BE");
			Calendar calendar = cultureInfo.Calendar;
			CalendarWeekRule calendarWeekRule = cultureInfo.DateTimeFormat.CalendarWeekRule;
			DayOfWeek firstDayOfWeek = cultureInfo.DateTimeFormat.FirstDayOfWeek;
			int weekOfYear = calendar.GetWeekOfYear(this.dtDatum, calendarWeekRule, firstDayOfWeek);
			DayOfWeek dayOfWeek = new DateTime(this.dtDatum.Year, 12, 31).DayOfWeek;
			if (weekOfYear == 1 && dayOfWeek != DayOfWeek.Sunday)
			{
				this.week = calendar.GetWeekOfYear(new DateTime(this.dtDatum.Year, 12, 31), calendarWeekRule, firstDayOfWeek);
				return;
			}
			this.week = weekOfYear;
		}
		public List<Record> omboekMeerUren()
		{
			List<Record> list = new List<Record>();
			int num = this.iUren;
			foreach (Record current in Record.lrRecords)
			{
				//current.sSalnum == this.sSalnum;
				if (current.sSalnum == this.sSalnum && current.sLooncode == "7010" && num > 0 && this.week == current.week && current.dtDatum.DayOfWeek != DayOfWeek.Sunday)
				{
					if (current.iUren == num)
					{
						current.iUren = 0;
						current.sLooncode = "0000";
						Record item = new Record(current.sSalnum, this.sWerkgeverNr, current.sDatum, this.sLooncode, num.ToString(), true);
						list.Add(item);
						num = 0;
					}
					else
					{
						if (current.iUren < num)
						{
							num -= current.iUren;
							int num2 = current.iUren;
							current.iUren = 0;
							current.sLooncode = "0000";
							Record item2 = new Record(current.sSalnum, this.sWerkgeverNr, current.sDatum, this.sLooncode, num2.ToString(), true);
							list.Add(item2);
						}
						else
						{
							current.iUren -= num;
							Record item3 = new Record(current.sSalnum, this.sWerkgeverNr, current.sDatum, this.sLooncode, num.ToString(), true);
							list.Add(item3);
							num = 0;
						}
					}
				}
			}
			this.iUren = 0;
			this.sLooncode = "0000";
			return list;
		}
		public void checkDubbel(List<Record> records)
		{
			foreach (Record current in records)
			{
				if (this.sDatum == current.sDatum && this.sLooncode == current.sLooncode && this.sSalnum == current.sSalnum)
				{
					string text = this.sSalnum;
					this.sSalnum = "weg";
					if (current.sSalnum == "weg")
					{
						this.sSalnum = text;
					}
					else
					{
						int num = current.iUren;
						int num2 = this.iUren;
						num += num2;
						current.iUren = num;
					}
				}
			}
		}
		public bool bestaatRecordZelfdeDag()
		{
			foreach (Record current in Record.lrRecords)
			{
				string text = this.sSalnum;
				this.sSalnum = "weg";
				int num = 1;
				if (current.sSalnum != "weg")
				{
					num = 0;
				}
				this.sSalnum = text;
				if (num == 0 && current.sSalnum == this.sSalnum && current.sDatum == this.sDatum)
				{
					return true;
				}
			}
			return false;
		}
		public int CompareTo(object obj)
		{
			Record record = (Record)obj;
			string text = this.sSalnum + this.sDatum;
			string strB = record.sSalnum + record.sDatum;
			int num = text.CompareTo(strB);
			if (num == 0)
			{
				num = text.CompareTo(strB);
			}
			return num;
		}
	}
}
