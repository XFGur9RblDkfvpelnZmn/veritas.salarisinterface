using Algemeen;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
namespace VER.SalarisInterface
{
	internal class Program
	{
		private static void Main(string[] args)
		{
			Parameter.SMandant = "bel";
			Log.Verwerking("Start Salarisverwerking");
			string sWerkgeverNr = Proces.SLeesFile(Proces.FiSqlsel("clientnum from msyspar")).Trim();
			string text = Proces.SLeesFile(Proces.FiSqlsel("compnr50 from msyspar")).Trim();
			int num = 29999999;
			int num2 = 0;
			string[] array = Regex.Split(Proces.SLeesFile(new FileInfo(Parameter.SSbRoot + "log\\expblox1.txt")), Environment.NewLine);
			for (int i = 0; i < array.Length; i++)
			{
				string text2 = array[i];
				if (text2.Length > 28)
				{
					if (Convert.ToInt32(text2.Substring(14, 6)) < num)
					{
						num = Convert.ToInt32(text2.Substring(14, 6));
					}
					else
					{
						if (Convert.ToInt32(text2.Substring(14, 6)) > num2)
						{
							num2 = Convert.ToInt32(text2.Substring(14, 6));
						}
					}
				}
			}
			Proces.dtStringToDate(num.ToString());
			Proces.dtStringToDate(num2.ToString());
			Dictionary<string, List<string>> dictionary = Proces.DlsSqlSel("catkod, catoms[7] from urencategorie");
			string arg_101_0 = string.Empty;
			string[] array2 = Regex.Split(Proces.SLeesFile(new FileInfo(Parameter.SSbRoot + "log\\expblox1.txt")), Environment.NewLine);
			for (int j = 0; j < array2.Length; j++)
			{
				string text3 = array2[j];
				try
				{
					string text4 = string.Empty;
					if (dictionary.ContainsKey(text3.Substring(22, 2)))
					{
						text4 = dictionary[text3.Substring(22, 2)][0];
					}
					if (text3.Substring(20, 4) == text)
					{
						text4 = text;
					}
					else
					{
						if (text3.Substring(20, 4) == "0000")
						{
							text4 = "0000";
						}
					}
					if (text4.Length == 4)
					{
						new Record(text3.Substring(7, 7), sWerkgeverNr, text3.Substring(14, 6), text4, text3.Substring(24, 4));
					}
				}
				catch (Exception ex)
				{
					if (text3.Trim().Length > 0)
					{
						Log.Verwerking("regel niet verwerkt: " + text3);
						Log.Exception(ex);
					}
				}
				finally
				{
					Proces.VerwijderWrkFiles();
				}
			}
			foreach (Record current in Record.lrRecords)
			{
				if (current.sLooncode == "1018" && current.dtDatum.DayOfWeek == DayOfWeek.Sunday)
				{
					Record.lrNewRecords.AddRange(current.omboekMeerUren());
				}
				Wochsoll wochsoll = Wochsoll.wZoekBijlage(current.sSalnum, current.week);
				if (wochsoll == null)
				{
					wochsoll = new Wochsoll(current.sSalnum, current.dtDatum);
				}
				if (wochsoll.wochsoll > wochsoll.filler)
				{
					int num3 = wochsoll.wochsoll - wochsoll.filler;
					foreach (Record current2 in Record.lrRecords)
					{
						if (current2.sSalnum == current.sSalnum && current2.sLooncode == "7010" && num3 > 0 && current2.week == current.week && current2.dtDatum.DayOfWeek != DayOfWeek.Sunday)
						{
							if (current2.iUren == num3)
							{
								current2.sLooncode = "1018";
								num3 = 0;
							}
							else
							{
								if (current2.iUren < num3)
								{
									current2.sLooncode = "1018";
									num3 -= current2.iUren;
								}
								else
								{
									current2.iUren -= num3;
									Record item = new Record(current2.sSalnum, current2.sWerkgeverNr, current2.sDatum, "1018", num3.ToString("0000"), true);
									Record.lrNewRecords.Add(item);
									num3 = 0;
								}
							}
						}
					}
					wochsoll.wochsoll = wochsoll.filler;
				}
			}
			Record.lrRecords.AddRange(Record.lrNewRecords);
			foreach (Record current3 in Record.lrRecords)
			{
				current3.checkDubbel(Record.lrRecords);
				if ((current3.sLooncode == "7010" || current3.sLooncode == "0000" || current3.sLooncode == "1010") && current3.iUren == 0 && current3.bestaatRecordZelfdeDag())
				{
					current3.sSalnum = "weg";
				}
                if (current3.sLooncode == "7010")
                    current3.sLooncode = "1010";
			}
			Record.lrRecords.Sort();
			FileInfo fileInfo = new FileInfo(Parameter.SSbRoot + "log\\expblox_vw.txt");
			TextWriter textWriter = new StreamWriter(fileInfo.FullName);
			foreach (Record current4 in Record.lrRecords)
			{
				if (current4.sSalnum != "weg")
				{
					textWriter.Write(string.Concat(new string[]
					{
						current4.sWerkgeverNr,
						current4.sSalnum,
						current4.dtDatum.ToString("yyMMdd"),
						current4.sLooncode,
						current4.iUren.ToString("0000"),
                        "               ",
						Environment.NewLine
					}));
				}
			}
			textWriter.Close();
			Proces.Tc_lp(fileInfo);
			Log.Verwerking("Einde Salarisinterface");
            Proces.VerwijderWrkFiles();
		}
	}
}
